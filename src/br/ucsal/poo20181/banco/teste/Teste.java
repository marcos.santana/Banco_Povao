package br.ucsal.poo20181.banco.teste;

import br.ucsal.poo20181.banco.domain.Conta;
import br.ucsal.poo20181.banco.domain.Correntista;

public class Teste {

	public static void main(String[] args) {
		
		Conta conta1= new Conta( new Correntista("12345678900","Marcos Santana", "Rua A", 500, "(71)0000-0000","1234568759", true, true));
		
		conta1.realizarDeposito(10);
		System.out.println(conta1.getSaldo());
		conta1.analisarCredito(true);
		conta1.realizarSaque(20);
		System.out.println(conta1.getSaldo());
		System.out.println(conta1.getLimCredito());
		System.out.println(conta1.getSituacao());
		conta1.bloquearConta(true, false);
		System.out.println(conta1.getSituacao());
		conta1.encerrarConta(true);
		conta1.realizarDeposito(50);
		System.out.println(conta1.getSaldo());
		conta1.encerrarConta(true);
		conta1.realizarSaque(40);
		conta1.encerrarConta(true);
		
		Conta conta2= new Conta();
	}

}
