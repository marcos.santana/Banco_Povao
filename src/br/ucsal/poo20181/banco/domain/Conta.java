package br.ucsal.poo20181.banco.domain;

public class Conta {
	private static int contador;
	private int numero;
	private double saldo;
	private double limCredito;
	private boolean crediEspecial;
	private String situacao;
	Correntista correntista;

	public Conta() {
		obterNumConta();
		obterSaldosConta();
		correntista = new Correntista();
	}

	public Conta(Correntista correntista) {
		this.obterNumConta();
		this.obterSaldosConta();
		this.correntista= correntista;
	}

	public void realizarDeposito(double valor) {
		if (valor > 0) {
			saldo += valor;
		} else {
			System.out.println("ISSO � LOUCURA");
		}
	}

	public void analisarCredito(boolean clienteQuerCredito) {
		if (correntista.isComproRenda()) {
			if (clienteQuerCredito) {
				crediEspecial = true;
				limCredito = 500;
			}
		} else {
			System.out.println("RENDA N�O COMPROVADA");
		}
	}

	public void realizarSaque(double valor) {
		if (valor <= saldo) {
			saldo -= valor;
		} else if (valor <= saldo + limCredito) {
			saldo -= valor;
			limCredito += saldo;
		} else {
			System.out.println("LIMITE N�O DISPONIVEL");
		}
	}

	public void bloquearConta(boolean justi�a, boolean problema) {
		if (justi�a) {
			System.out.println("CONTA BLOQUEADA A PEDIDO DA JUSTI�A");
			situacao = "BLOQUEADA";
		} else if (problema) {
			System.out.println("CONTA BLOQUEADA POR PROBLEMAS COM O CLIENTE");
			situacao = "BLOQUEADA";
		}
	}

	public void encerrarConta(boolean clienteQuerEncerrar) {
		if (clienteQuerEncerrar) {
			if (saldo < 0) {
				System.out.println("IMPOSS�VEL ENCERRAR CONTA POIS O SAlDO EST� NEGATIVO");
			} else if (saldo > 0) {
				System.out.println("CONTA AINDA POSSUI SALDO, PARA ENCERRAR REALIZE UM SAQUE NO VALOR DE " + saldo);
			} else {
				System.out.println("CONTA ENCERRADA COM SUCESSO");
				situacao="ENCERRADA";
			}
		}
	}

	public boolean isCrediEspecial() {
		return crediEspecial;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public int getNumero() {
		return numero;
	}

	public double getSaldo() {
		return saldo;
	}

	public double getLimCredito() {
		return limCredito;
	}

	private void obterNumConta() {
		contador++;
		numero = contador;
	}

	private void obterSaldosConta() {
		saldo = 0;
		limCredito = 0;
		crediEspecial = false;
		situacao = "ATIVA";
	}
}
