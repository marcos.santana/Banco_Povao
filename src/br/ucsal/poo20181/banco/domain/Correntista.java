package br.ucsal.poo20181.banco.domain;

import java.util.Scanner;

public class Correntista {
	private String cpf;
	private String nome;
	private String endereco;
	private double rendaMensal;
	private String telefone;
	
	private String rg;
	private boolean comproResid;
	private boolean comproRenda;
	Scanner sc = new Scanner(System.in);

	public Correntista() {
		getDados();
		getValidacaoDados();
	}

	public Correntista(String cpf, String nome, String endere�o, double rendaMensal, String telefone, String rg, boolean comproResid,
			boolean comproRenda) {
		this.cpf = cpf;
		this.nome = nome;
		this.endereco = endere�o;
		this.rendaMensal = rendaMensal;
		this.telefone=telefone;
		this.rg = rg;
		this.comproResid = comproResid;
		this.comproRenda = comproRenda;

	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndere�o() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public double getRendaMensal() {
		return rendaMensal;
	}

	public void setRendaMensal(double rendaMensal) {
		this.rendaMensal = rendaMensal;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public boolean isComproResid() {
		return comproResid;
	}

	public void setComproResid(boolean comproResid) {
		this.comproResid = comproResid;
	}

	public boolean isComproRenda() {
		return comproRenda;
	}

	public void setComproRenda(boolean comproRenda) {
		this.comproRenda = comproRenda;
	}

	private void getValidacaoDados() {
		System.out.println("Entre com o rg");
		rg = sc.next();
		while (rg == null) {
			System.out.println("Imposs�vel abrir conta, informe rg");
			System.out.println("Entre com o rg");
			rg = sc.next();
		}
		comproRenda = true;
		comproResid = true;
	}

	private void getDados() {
		System.out.println("Entre com o cpf");
		cpf = sc.nextLine();
		System.out.println("Entre com o nome");
		nome = sc.nextLine();
		System.out.println("Entre com o endere�o");
		endereco = sc.nextLine();
		System.out.println("Entre com o telefone");
		telefone = sc.nextLine();
		System.out.println("Entre com a rendaMensal");
		rendaMensal = sc.nextDouble();
	}
}
